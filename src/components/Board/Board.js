import React, { Component } from 'react';

import "components/Board/Board.css";

export default class Board extends Component {
    constructor(props) {
        super(props);
        this.availWidth = Math.round(window.innerWidth / 22);
        this.availHeight = Math.round((window.innerHeight - 42) / 19);
        this.arrX = [...Array(this.availWidth).keys()];
        this.arrY = [...Array(this.availHeight).keys()];
    }


    render() {
        return (
            <div className="board-wrapper">
                {
                    this.arrY.map((item, indexColumn) => <div className="column" column={indexColumn} key={indexColumn}>
                        {
                            this.arrX.map((item, indexRow) => <div className="cell" i={indexColumn} j={indexRow} key={indexRow}></div>)
                        }
                    </div>)
                }
            </div>
        )
    }
}
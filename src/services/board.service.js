class User {
    constructor(id) {
        this.id = id;
        this.coords = [];
        this.tail = null;
        this.flag = false;
    }

    start(coords) {
        let item = document.querySelectorAll(`[i="${coords.i}"][j="${coords.j}"]`)[0];
        this.startItem(item, this.id);
        this.tail = coords;
        this.coords.push(coords);
    }

    step(coords) {
        let item = document.querySelectorAll(`[i="${coords.i}"][j="${coords.j}"]`)[0];
        this.selectItem(item, this.id);
        this.tail = coords;
        this.coords.push(coords);
    }

    skip(coords) {
        let item = document.querySelectorAll(`[i="${coords.i}"][j="${coords.j}"]`)[0];
        this.skipItem(item, this.id);
        this.tail = coords;
        this.coords.push(coords);
    }

    tutorialExit(coords) {
        let item = document.querySelectorAll(`[i="${coords.i}"][j="${coords.j}"]`)[0];
        this.selectItem(item, this.id);
        this.colorSnakeOrange(this.id);
        this.tail = coords;
        this.coords.push(coords);
    }

    tutorialExitWithError(coords) {
        let item = document.querySelectorAll(`[i="${coords.i}"][j="${coords.j}"]`)[0];
        this.selectItem(item, this.id);
        this.colorSnakeRed(this.id);
        this.tail = coords;
        this.coords.push(coords);
    }

    startItem(item, id) {
        item.className = `cell selected-${id} snake-head selected`;
    }

    selectItem(item, id) {
        item.className = `cell selected-${id} selected`;
    }

    skipItem(item, id) {
        item.className = `cell selected-${id} selected skip`;
    }

    colorSnakeOrange(id) {
        let snakeItems = Array.prototype.slice.call(document.getElementsByClassName(`selected-${id}`));
        for (var i = 0; i < snakeItems.length; i++) {
            snakeItems[i].className = `cell selected-${id} selected exit`;
        }
    }

    colorSnakeRed(id) {
        let snakeItems = Array.prototype.slice.call(document.getElementsByClassName(`selected-${id}`));
        for (var i = 0; i < snakeItems.length; i++) {
            snakeItems[i].className = `cell selected-${id} selected error`;
        }
    }

    removeCoords() {
        this.coords = [];
    }
}

class BoardService {
    constructor() {
        this.users = [];
        this.w = Math.round(window.innerWidth / 22) - 1;
        this.h = Math.round((window.innerHeight - 42) / 19) - 1;
    }

    receiveData(data) {
        if (data.action === "tutorial_start") {
            this.startAction(data);
        }
        if (data.action === "tutorial_gotostep") {
            this.nextAction(data);
        }
        if (data.action === "tutorial_end") {
            this.endAction(data);
        }
        if (data.action === "tutorial_skipstep") {
            this.skipAction(data);
        }
        if (data.action === "tutorial_exit" && data.reference !== "error") {
            this.tutorialExitAction(data);
        }
        if (data.action === "tutorial_exit" && data.reference === "error") {
            this.tutorialExitWithErrorAction(data);
        }
    }

    startAction(data) {
        this.start(data.foreignuserid);
    }

    nextAction(data) {
        this.step(data.foreignuserid);
    }

    endAction(data) {
        this.end(data.foreignuserid);
    }

    skipAction(data) {
        this.skip(data.foreignuserid);
    }

    tutorialExitAction(data) {
        this.tutorialExit(data.foreignuserid);
    }

    tutorialExitWithErrorAction(data) {
        this.tutorialExitWithError(data.foreignuserid);
    }


    dropUser(id) {
        let index = this.users.findIndex((user) => user.id == id);
        this.users.splice(index, 1);
    }

    getUserById(id) {
        return this.users.find((a) => a.id === id);
    }

    getCoords(userId) {
        return this.users.find(user => user.id === userId);
    }


    randomCell() {
        let i = Math.floor(Math.random() * (0 - this.h + 1)) + this.h;
        let j = Math.floor(Math.random() * (0 - this.w + 1)) + this.w;
        return { i, j }
    }

    findCellValid(coords, usersSnakesCoords, randomArray) {
        let usersBlockedCoords = [];
        usersSnakesCoords.forEach(coord => {
            usersBlockedCoords.push(
                { i: coord.i, j: coord.j },
                { i: coord.i, j: coord.j + 1 },
                { i: coord.i, j: coord.j - 1 },
                { i: coord.i + 1, j: coord.j },
                { i: coord.i - 1, j: coord.j },
                { i: coord.i + 1, j: coord.j + 1 },
                { i: coord.i + 1, j: coord.j - 1 },
                { i: coord.i - 1, j: coord.j + 1 },
                { i: coord.i - 1, j: coord.j - 1 }
            );
        });
        return randomArray.filter((obj) => {
            let objArray = [
                { i: obj.coord.i, j: obj.coord.j },
                { i: obj.coord.i, j: obj.coord.j + 1 },
                { i: obj.coord.i, j: obj.coord.j - 1 },
                { i: obj.coord.i + 1, j: obj.coord.j },
                { i: obj.coord.i - 1, j: obj.coord.j },
                { i: obj.coord.i + 1, j: obj.coord.j + 1 },
                { i: obj.coord.i + 1, j: obj.coord.j - 1 },
                { i: obj.coord.i - 1, j: obj.coord.j + 1 },
                { i: obj.coord.i - 1, j: obj.coord.j - 1 }
            ].filter((obj) => {
                if (coords.findIndex(pair => pair.i === obj.i && pair.j === obj.j) < 0) {
                    return obj;
                }
            })
            if (this.intersection(objArray, usersSnakesCoords).length === 0) {
                return obj;
            }
        })
    }

    intersection(arr1, arr2) {
        let result = arr1.filter(n => {
            return arr2.findIndex(pair => pair.i === n.i && pair.j === n.j) > -1;
        });
        return result;
    }


    isRandomCellValid(usersSnakesCoords) {
        let obj = this.randomCell();
        let usersBlockedCoords = [];
        usersSnakesCoords.forEach(coord => {
            usersBlockedCoords.push(
                { i: coord.i, j: coord.j },
                { i: coord.i, j: coord.j + 1 },
                { i: coord.i, j: coord.j - 1 },
                { i: coord.i + 1, j: coord.j },
                { i: coord.i - 1, j: coord.j },
                { i: coord.i + 1, j: coord.j + 1 },
                { i: coord.i + 1, j: coord.j - 1 },
                { i: coord.i - 1, j: coord.j + 1 },
                { i: coord.i - 1, j: coord.j - 1 }
            );
        });
        while (usersBlockedCoords.findIndex(pair => pair.i === obj.i && pair.j === obj.j) > 0) {
            obj = this.randomCell();
        }
        return obj;
    }

    checkAvailableCellForHead(user) {
        let usersSnakesCoords = [];
        this.users.forEach((user) => {
            user.coords.forEach((coords) => {
                usersSnakesCoords.push(coords);
            })
        });
        if (usersSnakesCoords.length > 0) {
            let coords = this.isRandomCellValid(usersSnakesCoords);
            user.start(coords)
        }
        else {
            let coords = this.randomCell();
            user.start(coords);
        }
    }

    checkAvailableCellForStep(user) {
        let usersSnakesCoords = [];
        this.users.forEach((user) => {
            user.coords.forEach((coords) => {
                usersSnakesCoords.push(coords);
            })
        });
        if (usersSnakesCoords.length === 0) {
            return;
        }
        let tail = user.coords[user.coords.length - 1];
        let row = tail ? tail.i : 0;
        let column = tail ? tail.j : 0;

        let top = row != 0 ? { i: row - 1, j: column } : 0;
        let bottom = row != this.h ? { i: row + 1, j: column } : this.h;
        let left = column != 0 ? { i: row, j: column - 1 } : 0;
        let right = column != this.w ? { i: row, j: column + 1 } : this.w;

        let leftAv = usersSnakesCoords.findIndex((coord) => {
            if (left !== 0 && left.i === coord.i && left.j === coord.j) {
                return true;
            }
        }) < 0;
        let rightAv = usersSnakesCoords.findIndex((coord) => {
            if (right !== this.w && right.i === coord.i && right.j === coord.j) {
                return true;
            }
        }) < 0;

        let topAv = usersSnakesCoords.findIndex((coord) => {
            if (top !== 0 && top.i === coord.i && top.j === coord.j) {
                return true;
            }
        }) < 0;

        let botAv = usersSnakesCoords.findIndex((coord) => {
            if (bottom !== this.h && bottom.i === coord.i && bottom.j === coord.j) {
                return true;
            }
        }) < 0;

        let randomArray = [
            { direction: 'left', value: leftAv, coord: left },
            { direction: 'right', value: rightAv, coord: right },
            { direction: 'top', value: topAv, coord: top },
            { direction: 'bottom', value: botAv, coord: bottom }
        ].filter((item) => item.value && typeof item.coord === "object");
        if (randomArray.length > 0) {
            console.log('random array', randomArray);
            let validCell = this.findCellValid(user.coords, usersSnakesCoords, randomArray);
            let newItem = validCell[Math.floor(Math.random() * validCell.length)].coord; return newItem;
        } else {
            console.log('snake doesnt have available cell');
            user.removeCoords();
            return;
        }
    }

    start(userId) {
        if (this.getUserById(userId)) {
            return;
        }
        const user = new User(userId);
        this.users.push(user);
        this.checkAvailableCellForHead(user);
    }

    step(userId) {
        const user = this.getUserById(userId);
        if (!user) {
            return;
        }
        let validCell = this.checkAvailableCellForStep(user);
        user.step(validCell);
        user.flag = false;
    }


    end(userId) {
        const user = this.getUserById(userId);
        if (!user) {
            return;
        }
        let validCell = this.checkAvailableCellForStep(user);
        user.step(validCell);
        //todo separate method

        let snakeItems = Array.prototype.slice.call(document.getElementsByClassName(`selected-${userId}`));
        for (var i = 0; i < snakeItems.length; i++) {
            snakeItems[i].className = `cell selected-${userId} selected tutorial-end`;
        }
        setTimeout(() => {
            for (var i = 0; i < snakeItems.length; i++) {
                snakeItems[i].className = `cell`;
            }
            user.removeCoords();
            this.dropUser(userId)
        }, 2500);
    }

    skip(userId) {
        const user = this.getUserById(userId);
        if (!user) {
            return;
        }
        let validCell = this.checkAvailableCellForStep(user);
        user.skip(validCell);
    }

    tutorialExit(userId) {
        let user = this.getUserById(userId);
        if (!user) {
            return;
        }
        user.flag = true;
        setTimeout(() => {
            if (user.flag) {
                let snakeItems = Array.prototype.slice.call(document.getElementsByClassName(`selected-${userId}`));
                for (var i = 0; i < snakeItems.length; i++) {
                    snakeItems[i].className = `cell`;
                }
                this.dropUser(userId);
                user.flag = false;
            }
            else {
                let snakeItems = Array.prototype.slice.call(document.getElementsByClassName(`selected-${userId}`));
                for (var i = 0; i < snakeItems.length; i++) {
                    snakeItems[i].className = `cell selected-${userId} selected exit-continue`;
                }
                let coords = this.getCoords(userId).coords[0];
                let item = document.querySelectorAll(`[i="${coords.i}"][j="${coords.j}"]`)[0];
                item.classList.remove('exit-continue');
                item.classList.add('snake-head');
            }
        }, 10000);
        let validCell = this.checkAvailableCellForStep(user);
        user.tutorialExit(validCell);
    }

    tutorialExitWithError(userId) {
        let user = this.getUserById(userId);
        if (!user) {
            return;
        }
        user.flag = true;
        setTimeout(() => {
            if (user.flag) {
                let snakeItems = Array.prototype.slice.call(document.getElementsByClassName(`selected-${userId}`));
                for (var i = 0; i < snakeItems.length; i++) {
                    snakeItems[i].className = `cell`;
                }
                this.dropUser(userId);
                user.flag = false;
            }
            else {
                let snakeItems = Array.prototype.slice.call(document.getElementsByClassName(`selected-${userId}`));
                for (var i = 0; i < snakeItems.length; i++) {
                    snakeItems[i].className = `cell selected-${userId} selected error-continue`;
                }
                let coords = this.getCoords(userId).coords[0];
                let item = document.querySelectorAll(`[i="${coords.i}"][j="${coords.j}"]`)[0];
                item.classList.remove('error-continue');
                item.classList.add('snake-head');
            }
        }, 10000);
        let validCell = this.checkAvailableCellForStep(user);
        user.tutorialExitWithError(validCell);
    }
}

const boardService = new BoardService();
export default boardService;
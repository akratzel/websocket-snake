import React, { Component } from 'react';
import socketIOClient from "socket.io-client";
import boardService from "services/board.service";

import Board from "components/Board/Board";

class App extends Component {
    constructor(props) {
        super(props);
        this.socket = socketIOClient("https://websocket-gate.userlane.com/")
        this.data = [];
    }

    initSocket = () => {
        if (this.socket)
            this.socket.destroy();
        this.socket = socketIOClient("https://websocket-gate.userlane.com/")
        this.socket.on('analytics_event', (data) => {
            this.data = data.body.data;
            boardService.receiveData(this.data);
            console.log('analytics_event', data);
        });
        this.socket.on('authentication_successful', (data) => {
            console.log('Successful auth, yay');
        });
        this.socket.on('disconnect', () => {
            this.initSocket();
            this.sendtoken();
        });
        this.socket.on('authentication_required', () => {
            this.sendtoken();
        });
    }

    sendtoken = (token) => {
        this.socket.emit('authenticate', { token });
    }

    submittoken() {
        this.initSocket();
        this.sendtoken('BLWmpPZdm19eFEIaJ8tf1xoi95Ffqbs34QHZaDeYglGyknBrA1zeDf8u6HwvkhzITBtm9sj2dA8yAFUbaBWKSc2UW8vNA77oyGmyqg1czillik9LvbML4aLQETGDttn3');
        return false;
    }

    componentDidMount() {
        this.submittoken();
    }

    render() {
        return (
            <div className="App">
                <Board />
            </div>
        );
    }
}

export default App;

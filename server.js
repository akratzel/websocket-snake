const stompit = require('stompit');
const PORT = 8000;
const HOST = '0.0.0.0';
const APIHOST = process.env.API_HOST || 'https://staging.userlane.com/';
const stompConnectOptions = {
    'host': process.env.STOMP_HOST || 'rabbitmq-staging.usln.rocks',
    'port': process.env.STOMP_PORT || 61613,
    'connectHeaders': {
        'login': process.env.STOMP_USER || 'usr-wsg',
        'passcode': process.env.STOMP_PW || 'TIbWlK9gjkxaACZ219ZP',
        'heart-beat': '5000,5000'
    },
    'ssl': false
};

const BOT_TOKEN = process.env.BOT_TOKEN || false;
const fetch = require('node-fetch');
const app = require('http').createServer(handler);
const io = require('socket.io')(app);
const fs = require('fs');

app.listen(PORT);

function handler(req, res) {
    let url;
    if (req.url === '/') {
        url = '/build/index.html';
    }
    else {
        url = `/build${req.url}`;
    }
    fs.readFile(__dirname + url,
        function (err, data) {
            if (err) {
                res.writeHead(500);
                return res.end('Error loading index.html');
            }
    
        res.writeHead(200);
        res.end(data);
    });
}


io.on('connection', function (socket) {
    console.log('new connection here yay');

    socket.emit('authentication_required', {});

    var is_authenticated = false;

    socket.on('authenticate', function (data) {
        console.log('data ======> ', data)
        console.log('authentication request', data);
        if(!is_authenticated) {
            is_authenticated = true;
            tryAuthenticate(data.token).then(res => {
                if(res === false){
                    console.log('request is not authenticated');
                    socket.emit('authentication_required', {});
                    is_authenticated = false;
                }
                else{
                    // authenticated
                    console.log('final pass, socket ', socket.id, 'is now authenticated', res.email);
                    socket.emit('authentication_successful', res);
                    startRelayStompMessages(socket);
                }
            }).catch(function(error){
                console.error('ERROR!', error);
                socket.emit('authentication_required', {});
                is_authenticated = false;
            });
        }else{
            console.log('Auth request ignored');
        }
    });
});



function tryAuthenticate(token){
    console.log("token", token);
    return new Promise(function (resolve, reject) {
        if (BOT_TOKEN && token === BOT_TOKEN){
            resolve({
                "id": -1,
                "name": "Boten Anna",
                "email": "bot",
                "isSuperAdmin": true,
                "isEnabled": true
            });
        }else{
            fetch(`${APIHOST}v2/users/me`, {
                headers: {'Authorization': token},
            }).then(res => {
                if (res.status === 200) {
                    console.log('request is authenticated');
                    res.json().then(body => {
                        console.log('response body', body);
                        if (body.isEnabled && body.isSuperAdmin) {
                            resolve(body);
                        }
                    }).catch(reject);
                } else {
                    resolve(false);
                }
            }).catch(reject);
        }
    });
}


function startRelayStompMessages(socket) {
    console.log('socket', socket.id, "connecting to Stomp...");

    // connect to queue and forward events
    stompit.connect(stompConnectOptions, function (error, client) {
        if (error || !client) {
            console.log('stomp connect error ', error, error.message, client);
            return;
        }

        const subscribeHeaders = {
            'destination': '/topic/analytics_events.#',
            'ack': 'client-individual'
        };

        console.log('socket', socket.id, "connected to Stomp...");

        client.subscribe(subscribeHeaders, function (error, message) {

            if (error) {
                console.log('subscribe error ' + error.message);
                return;
            }

            message.readString('utf-8', function (error, body) {

                if (error) {
                    console.log('read message error ' + error.message);
                    return;
                }

                try {
                    body = JSON.parse(body);
                } catch (e) {
                    console.log('error parsing json', body);
                }

                socket.emit('analytics_event',{ body });
                console.log('received message',body.data);

                client.ack(message);
            });
        });

        socket.on('disconnect', function () {
            console.log('socket',socket.id,'disconnected');
            client.disconnect();
        })
    });


}

